<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Conference;
use App\Form\CommentFormType;
use App\Repository\CommentRepository;
use App\Repository\ConferenceRepository;
use App\Spam\SpamChecker;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class ConferenceController extends AbstractController
{
    private Environment            $twig;
    private EntityManagerInterface $entityManager;

    public function __construct(Environment $twig, EntityManagerInterface $entityManager)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index(ConferenceRepository $conferenceRepository): Response
    {
        return new Response(
            $this->twig->render(
                'conference/index.html.twig',
                [
                    'conferences' => $conferenceRepository->findAll(),
                ]
            )
        );
    }

    /**
     * @Route("/conference/{slug}", name="conference")
     */
    public function show(
        Request $request,
        Conference $conference,
        CommentRepository $commentRepository,
        SpamChecker $spamChecker,
        string $photoDir
    ): Response {
        $comment = new Comment();
        $form = $this->createForm(CommentFormType::class, $comment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveCommentAndPhoto($request, $comment, $conference, $form, $spamChecker, $photoDir);

            return $this->redirectToRoute('conference', ['slug' => $conference->getSlug()]);
        }

        $offset = max(0, $request->query->getInt('offset'));
        $paginator = $commentRepository->getCommentPaginator($conference, $offset);

        return new Response(
            $this->twig->render(
                'conference/show.html.twig',
                [
                    'conference' => $conference,
                    'comments' => $paginator,
                    'previous' => $offset - CommentRepository::PAGINATOR_PER_PAGE,
                    'next' => min(count($paginator), $offset + CommentRepository::PAGINATOR_PER_PAGE),
                    'comment_form' => $form->createView(),
                ]
            )
        );
    }

    /**
     * @throws Exception
     */
    private function saveCommentAndPhoto(
        Request $request,
        Comment $comment,
        Conference $conference,
        FormInterface $form,
        SpamChecker $spamChecker,
        string $photoDir
    ): void {
        $comment->setConference($conference);
        /** @var UploadedFile|null $photo */
        $photo = $form['photo']->getData();
        if ($photo) {
            $filename = bin2hex(random_bytes(6)).'.'.$photo->guessExtension();
            try {
                $photo->move($photoDir, $filename);
            } catch (FileException $ignored) {
                // Here we do nothing.
            }
            $comment->setPhotoFilename($filename);
        }
        $this->entityManager->persist($comment);
        if ($spamChecker->getSpamScore(
            $comment,
            [
                'user_ip' => $request->getClientIp(),
                'user_agent' => $request->headers->get('user-agent'),
                'referrer' => $request->headers->get('referer'),
                'permalink' => $request->getUri(),
            ]
        )->isSpam()) {
            throw new RuntimeException('Blatant spam, go away');
        }

        $this->entityManager->flush();
    }
}
