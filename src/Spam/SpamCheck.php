<?php

namespace App\Spam;

class SpamCheck
{
    private int $score;

    private function __construct(int $score)
    {
        $this->score = $score;
    }

    public static function blatantSpam(): SpamCheck
    {
        return new self(2);
    }

    public static function possibleSpam(): SpamCheck
    {
        return new self(1);
    }

    public static function unlikelySpam(): SpamCheck
    {
        return new self(0);
    }

    public function isSpam(): bool
    {
        return $this->isPossibleSpam() || $this->isBlatantSpam();
    }

    public function isPossibleSpam(): bool
    {
        return 1 === $this->score;
    }

    public function isBlatantSpam(): bool
    {
        return 2 === $this->score;
    }

    public function isUnlikelySpam(): bool
    {
        return 0 === $this->score;
    }
}
