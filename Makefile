.PHONY: config install start stop restart recert cert migration sql AdminUser onCreate
.SILENT:

include .env
include .env.local

DC = docker-compose

help:  ## Displays this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

install: ## Builds the docker images
	$(DC) build

##@ Daily use

restart: stop start ## Restarts the containers

composer.lock: composer.json bin/symfony
	bin/symfony composer update --lock --no-scripts --no-interaction


vendor: composer.lock bin/symfony ## build the vendors
	bin/symfony composer install

##@ Administration

migration: bin/symfony ## migrate the db
	bin/symfony console doctrine:migration:migrate -qn


recert: stop uncert cert start ## redoes the certification

uncert: bin/symfony
	bin/symfony server:ca:install

cert: bin/symfony
	bin/symfony server:ca:install

sql: ## starts a pgsql client
	bin/symfony run psql

bin/symfony: var/bin/symfonyInstaller ## fetch and install the symfony tool
	var/bin/symfonyInstaller --install-dir bin

var/bin/symfonyInstaller:
	mkdir -p var/bin
	wget https://get.symfony.com/cli/installer -O var/bin/symfonyInstaller
	chmod a+x var/bin/symfonyInstaller

AdminUser: bin/symfony ## Register a new admin user
	bin/createAdminUser

.env.local:
	touch .env.local


start:## Launchs the docker containers
	bin/symfony serve -d
	docker-compose up -d

stop:## Stops and destroys the containers
	bin/symfony server:stop
	$(DC) stop

down:
	$(DC) down

onCreate: migration AdminUser