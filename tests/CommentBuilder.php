<?php

namespace App\Tests;

use App\Entity\Comment;
use DateTimeInterface;

class CommentBuilder
{
    private Comment $comment;

    private function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public static function default(): CommentBuilder
    {
        $comment = new Comment();
        $comment->setCreatedAtValue();

        return (new self($comment))
            ->author('::author::')
            ->email('::email::')
            ->text('::text::');
    }

    private function text(string $aText): CommentBuilder
    {
        $this->comment->setText($aText);

        return $this;
    }

    public function email(string $email): CommentBuilder
    {
        $this->comment->setEmail($email);

        return $this;
    }

    public function author(string $author): CommentBuilder
    {
        $this->comment->setAuthor($author);

        return $this;
    }

    public function date(DateTimeInterface $creationDateTime): CommentBuilder
    {
        $this->comment->setCreatedAt($creationDateTime);

        return $this;
    }

    public function get(): Comment
    {
        return $this->comment;
    }
}
