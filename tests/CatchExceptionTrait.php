<?php

namespace App\Tests;

use Exception;

trait CatchExceptionTrait
{
    public function catchException(callable $lambda): ?Exception
    {
        try {
            $lambda();

            return null;
        } catch (Exception $e) {
            return $e;
        }
    }
}
