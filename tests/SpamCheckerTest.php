<?php

namespace App\Tests;

use App\Spam\SpamCheck;
use App\Spam\SpamChecker;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class SpamCheckerTest extends TestCase
{
    use CatchExceptionTrait;

    public function testSpamScoreWithInvalidRequest()
    {
        // Given
        $comment = CommentBuilder::default()->get();
        $context = [];
        $client = new MockHttpClient(
            new MockResponse('invalid', ['response_headers' => ['x-akismet-debug-help: Invalid key']])
        );
        $checker = new SpamChecker($client, 'abcde');

        // When
        $exception = $this->catchException(fn() => $checker->getSpamScore($comment, $context));

        // Then
        $this->assertEquals(new RuntimeException('Unable to check for spam: invalid (Invalid key)'), $exception);
    }

    public function testBlatantSpam()
    {
        // Given
        $comment = CommentBuilder::default()->get();
        $context = [];
        $response = new MockResponse('', ['response_headers' => ['x-akismet-pro-tip: discard']]);
        $client = new MockHttpClient($response);
        $checker = new SpamChecker($client, 'abcde');

        // When
        $score = $checker->getSpamScore($comment, $context);

        // Then
        $this->assertEquals(SpamCheck::blatantSpam(), $score);
    }


    public function testLikelySpam()
    {
        // Given
        $comment = CommentBuilder::default()->get();
        $context = [];
        $client = new MockHttpClient(new MockResponse('true'));
        $checker = new SpamChecker($client, 'abcde');

        // When
        $score = $checker->getSpamScore($comment, $context);

        // Then
        $this->assertEquals(SpamCheck::possibleSpam(), $score);
    }

    public function testUnlikelySpam()
    {
        // Given
        $comment = CommentBuilder::default()->get();
        $context = [];
        $client = new MockHttpClient(new MockResponse('false'));
        $checker = new SpamChecker($client, 'abcde');

        // When
        $score = $checker->getSpamScore($comment, $context);

        // Then
        $this->assertEquals(SpamCheck::unlikelySpam(), $score);
    }
}
